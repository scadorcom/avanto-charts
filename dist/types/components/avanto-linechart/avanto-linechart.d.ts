import '../../stencil.core';
export declare class AvantoLinechart {
    data: number[];
    private element;
    protected componentDidLoad(): void;
    render(): JSX.Element;
}
