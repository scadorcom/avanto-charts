import * as chartjs from 'chart.js';
const { Chart } = chartjs.default.Chart;
export class AvantoLinechart {
    constructor() {
        this.data = [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478];
    }
    componentDidLoad() {
        this.data = [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478];
        var canvas = this.element.shadowRoot.querySelector('canvas');
        var ctr = 0;
        var labels = this.data.map(() => ctr++);
        var opts = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                        data: this.data,
                        label: "",
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: "rgba(0,85,145,0.2)",
                        borderColor: "rgb(0,85,145)",
                        spanGaps: false
                    }
                ]
            },
            options: {
                elements: {
                    point: { radius: 0 },
                    line: { borderWidth: 1 }
                },
                legend: { display: false },
                tooltips: { enabled: false },
                scales: {
                    xAxes: [{ display: false }],
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: true,
                            },
                        }
                    ]
                },
                layout: { padding: { left: 15, right: 15, top: 0, bottom: 10 } },
                animation: {
                    duration: 0
                }
            }
        };
        new Chart(canvas, opts);
    }
    render() {
        return h("div", { class: "chartbox" },
            h("canvas", { height: "75" }));
    }
    static get is() { return "avanto-linechart"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "data": {
            "type": "Any",
            "attr": "data"
        },
        "element": {
            "elementRef": true
        }
    }; }
    static get style() { return "/**style-placeholder:avanto-linechart:**/"; }
}
