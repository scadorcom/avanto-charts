
// avantochart: Custom Elements Define Library, ES Module/es5 Target

import { defineCustomElement } from './avantochart.core.js';
import { COMPONENTS } from './avantochart.components.js';

export function defineCustomElements(win, opts) {
  return defineCustomElement(win, COMPONENTS, opts);
}
