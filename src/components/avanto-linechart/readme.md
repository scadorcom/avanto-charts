# avanto-linechart



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type       | Default                                              |
| -------- | --------- | ----------- | ---------- | ---------------------------------------------------- |
| `data`   | --        |             | `number[]` | `[86, 114, 106, 106, 107, 111, 133, 221, 783, 2478]` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
