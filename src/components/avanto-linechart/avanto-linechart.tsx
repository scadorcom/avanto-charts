import { Component, Prop, Element } from '@stencil/core';
//import { Chart } from 'chart.js';


import * as chartjs from 'chart.js';
const { Chart } = chartjs.default.Chart;

@Component({
	tag: 'avanto-linechart',
	styleUrl: 'avanto-linechart.css',
	shadow: true
})
export class AvantoLinechart {

	@Prop() data: number[] = [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478];

	@Element() private element: HTMLElement;

	protected componentDidLoad(): void {

		this.data = [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478];
		var canvas = this.element.shadowRoot.querySelector('canvas');
		var ctr = 0;
		var labels = this.data.map(() => ctr++);

		var opts = {
			type: 'line',
			data: {
				labels: labels,
				datasets: [{
					data: this.data,
					label: "",
					fill: true,
					lineTension: 0.1,
					backgroundColor: "rgba(0,85,145,0.2)", // Linde dark blue - tint 20%
					borderColor: "rgb(0,85,145)",    // Linde dark blue
					spanGaps: false
				}
				]
			},
			options: {
				elements: {
					point: { radius: 0 },
					line: { borderWidth: 1 }
				},
				legend: { display: false },
				tooltips: { enabled: false },
				scales: {
					xAxes: [{ display: false }], 
					yAxes: [
						{
							ticks: {
								beginAtZero: true
							},
							gridLines: {
								display: true,
							},

						}
					]
				},
				layout: { padding: { left: 15, right: 15, top: 0, bottom: 10 } },
				animation: {
					duration: 0
				}
			}
		}
		new Chart(canvas, opts);
	}

	render() {
		return <div class="chartbox"><canvas height="75"></canvas></div>;
	}
}
